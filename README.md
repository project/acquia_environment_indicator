# Acquia Cloud Environment Indicator

Module that displays Acquia Environnement Indicator in Drupal's
admin toolbar.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/acquia_environment_indicator).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/acquia_environment_indicator).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module works in an Acquia Cloud Platform or Site Factory contexts.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

`composer config repositories.drupal composer https://packages.drupal.org/9`
`composer require drupal/acquia_environment_indicator:~1`


## Configuration

1. No configuration needed.
2. You can choose to enable or disable the environment indicator from the admin interface.


## Maintainers

- Issam Feddi - [ifeddi](https://www.drupal.org/u/ifeddi)
- Iyas Hamadet - [ihamadet](https://www.drupal.org/u/ihamadet)
